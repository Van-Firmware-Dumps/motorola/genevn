#
# Copyright (C) 2025 The Android Open Source Project
# Copyright (C) 2025 SebaUbuntu's TWRP device tree generator
#
# SPDX-License-Identifier: Apache-2.0
#

add_lunch_combo omni_genevn-user
add_lunch_combo omni_genevn-userdebug
add_lunch_combo omni_genevn-eng
