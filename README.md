## user 14 U1TGNS34.42-86-9 b1b4b release-keys
- Manufacturer: motorola
- Platform: parrot
- Codename: genevn
- Brand: motorola
- Flavor: user
- Release Version: 14
- Kernel Version: 5.10.218
- Id: U1TGNS34.42-86-9
- Incremental: b1b4b
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: true
- Treble Device: true
- Locale: en-US
- Screen Density: undefined
- Fingerprint: motorola/genevn/genevn:12/U1TGNS34.42-86-9/b1b4b:user/release-keys
- OTA version: 
- Branch: user-14-U1TGNS34.42-86-9-b1b4b-release-keys-23675
- Repo: motorola/genevn
